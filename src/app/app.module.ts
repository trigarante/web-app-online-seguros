import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatCardModule } from '@angular/material/card';
import { MatMenuModule } from '@angular/material/menu';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { LayoutModule } from '@angular/cdk/layout';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatFormFieldModule} from '@angular/material/form-field';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {HttpClientModule} from '@angular/common/http';
import {DashboardModule} from './modulos/dashboard/dashboard.module';
import {AuthModule} from './modulos/auth/auth.module';
import {SharedModule} from './shared/shared.module';
import {NotFoundComponent} from './modulos/not-found/not-found.component';
import {AvisoDePrivacidadComponent} from './modulos/aviso-de-privacidad/aviso-de-privacidad.component';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {AclaracionYSugerenciasModule} from "./modulos/aclaracion-y-sugerencias/aclaracion-y-sugerencias.module";
import {CancelarUnaPolizaModule} from "./modulos/cancelar-una-poliza/cancelar-una-poliza.module";
import {CatalogoDeFacturacionModule} from "./modulos/catalogo-de-facturacion/catalogo-de-facturacion.module";
import {IdentificacionOficialModule} from "./modulos/identificacion-oficial/identificacion-oficial.module";
import {InspeccionVehicularModule} from "./modulos/inspeccion-vehicular/inspeccion-vehicular.module";
import {InstruccionesDePagoModule} from "./modulos/instrucciones-de-pago/instrucciones-de-pago.module";
import {SiniestrosModule} from "./modulos/siniestros/siniestros.module";

@NgModule({
  declarations: [
    AppComponent,
    NotFoundComponent,
    AvisoDePrivacidadComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatGridListModule,
    MatCardModule,
    MatMenuModule,
    MatIconModule,
    MatButtonModule,
    LayoutModule,
    MatToolbarModule,
    MatFormFieldModule,
    ReactiveFormsModule,
    MatInputModule,
    MatCheckboxModule,
    HttpClientModule,
    DashboardModule,
    AuthModule,
    SharedModule,
    FormsModule,
    PdfViewerModule,
    AclaracionYSugerenciasModule,
    CancelarUnaPolizaModule,
    CatalogoDeFacturacionModule,
    IdentificacionOficialModule,
    InspeccionVehicularModule,
    InstruccionesDePagoModule,
    SiniestrosModule,
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
