import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {IdentificacionOficialPageComponent} from './containers';

const routes: Routes = [
  {
    path: '',
    component: IdentificacionOficialPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class IdentificacionOficialRoutingModule { }
