import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { IdentificacionOficialRoutingModule } from './identificacion-oficial-routing.module';
import { IdentificacionOficialComponent } from './components/identificacion-oficial/identificacion-oficial.component';
import { IdentificacionOficialPageComponent } from './containers/identificacion-oficial-page/identificacion-oficial-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatListModule} from '@angular/material/list';


@NgModule({
  declarations: [IdentificacionOficialComponent, IdentificacionOficialPageComponent],
  imports: [
    CommonModule,
    IdentificacionOficialRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule,
    MatListModule
  ]
})
export class IdentificacionOficialModule { }
