import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IdentificacionOficialPageComponent } from './identificacion-oficial-page.component';

describe('IdentificacionOficialPageComponent', () => {
  let component: IdentificacionOficialPageComponent;
  let fixture: ComponentFixture<IdentificacionOficialPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IdentificacionOficialPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IdentificacionOficialPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
