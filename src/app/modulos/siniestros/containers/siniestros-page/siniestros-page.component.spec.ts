import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SiniestrosPageComponent } from './siniestros-page.component';

describe('SiniestrosPageComponent', () => {
  let component: SiniestrosPageComponent;
  let fixture: ComponentFixture<SiniestrosPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SiniestrosPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SiniestrosPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
