import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {SiniestrosPageComponent} from './containers';

const routes: Routes = [
  {
    path: '',
    component: SiniestrosPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SiniestrosRoutingModule { }
