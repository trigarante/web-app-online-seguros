import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';

import { routes } from '../../../consts';

@Injectable()
export class AuthGuard implements CanActivate{
  public routers: typeof routes = routes;

  constructor(private router: Router) {
  }

  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): boolean {
    const token = JSON.parse(localStorage.getItem('token'));
    const aviso = localStorage.getItem('aviso');

    if (aviso !== 'true') {// Se valida que el usuario acepte el aviso de privacidad
      this.router.navigate([this.routers.AVISO_PRIVACIDAD]);
    }
    if (token && token.tipo === 1) {
      return  true;
    } else {
      this.router.navigate([this.routers.LOGIN]);
      return  false;
    }
  }
}
