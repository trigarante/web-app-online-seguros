import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthService} from '../../services';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.scss']
})
export class LoginFormComponent implements OnInit {
  loginForm: FormGroup;
  hide = true;
  result = '';
  constructor(private fb: FormBuilder, private authService: AuthService) {
    this.loginForm = this.fb.group({
      email: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      contrasenaApp: new FormControl('', Validators.compose([Validators.required, /*Validators.pattern(this.decimal)*/])),
      activo: 1,
      idMarcaEmpresa: 4
    });
  }
  ngOnInit() {
  }

  iniciarSesion(): void{
    if (this.loginForm.invalid) {
      return;
    }
    this.authService.auth(this.loginForm.value).catch((result) => {
      this.result = 'Parece que tus datos son incorrectos. Por favor intentalo nuevamente.';
    });
  }
}
