export interface Usuario {
    contrasenaApp: string;
    email: string;
}
export interface ResponseApiData {
  id: number;
  tipo: number;
  msg: string;
}
