import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ClienteService } from 'src/app/@core/services/cliente.service';
import { Usuario } from 'src/app/modulos/auth/models/usuario';
import { AuthService } from "../../../auth/services";

@Component({
  selector: 'app-ajustes',
  templateUrl: './ajustes.component.html',
  styleUrls: ['./ajustes.component.scss']
})
export class AjustesComponent implements OnInit {

  hide = true;
  hide2 = true;

  formGroup: FormGroup;
  confirmChangePass: boolean = false;
  static clic: number = 0;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private clienteService: ClienteService) { }

  ngOnInit() {
    this.startBuildForm();
  }

  passwordsIguales(pass1Name: string, pass2Name: string) {

    return (formGroup: FormGroup) => {

      const pass1Control = formGroup.get(pass1Name);
      const pass2Control = formGroup.get(pass2Name);

      if (pass1Control.value === pass2Control.value) {
        pass2Control.setErrors(null);
      } else {
        pass2Control.setErrors({ noEsIgual: true });
      }
    };
  }

  confirmPassword() {
    if (this.formGroup.valid) {
      if (this.passwordField.value === this.confirmPasswordField.value) {
        this.confirmChangePass = true;
        AjustesComponent.clic++;
        if (AjustesComponent.clic > 1) {
          if (this.currentPasswordField.value === '' || this.currentPasswordField.value.trim() === '') {
            alert('El campo de contraseña actual es obligatorio');
          } else {
            const loginData: Usuario = {
              email: localStorage.getItem('userEmail'),
              contrasenaApp: this.currentPasswordField.value
            };
            console.log(loginData);
            this.authService.loginUser(loginData)
              .subscribe((response) => {
                console.log('Response => ', response);
              }, (error) => {
                console.log('Error => ', error);
                alert('La contraseña es Incorrecta.\nIntente de nuevo.');
              }, () => {
                this.updatePasswordMethod();
              })
          }
        }
      } else {
        alert('Las contraseñas no son iguales');
        this.confirmChangePass = false;
        AjustesComponent.clic--;
      }
    }
  }

  private updatePasswordMethod() {
    const idUser = parseInt(JSON.parse(localStorage.getItem('token')).id)
    this.clienteService.putUpdatePassword(this.confirmPasswordField.value, idUser)
      .subscribe((data) => {
        console.log('Data => ', data);
      }, (error) => {
        console.log('Error =>', error);
      }, () => {
        alert('La contraseña se actualizó correctamente');
        this.formGroup.setValue({ password: '', confirmPassword: '', currentPassword: '' });
        AjustesComponent.clic = 0;
      })
  }

  confirmPassword2() {
    const cstmPassword = localStorage.getItem('customPassword');
    const idUser = parseInt(JSON.parse(localStorage.getItem('token')).id)
    if (this.formGroup.valid) {
      if (this.passwordField.value === this.confirmPasswordField.value) {
        this.confirmChangePass = true;
        AjustesComponent.clic++;
        if (AjustesComponent.clic > 1) {
          if (this.currentPasswordField.value === '' || this.currentPasswordField.value.trim() === '') {
            alert('El campo de contraseña actual es obligatorio');
          } else {



            if (this.currentPasswordField.value === cstmPassword) {
              // Aqì va a llamarse al método para hacer el update de la password
              this.clienteService.putUpdatePassword(this.confirmPasswordField.value, idUser)
                .subscribe((data) => {
                  console.log('Data => ', data);
                }, (error) => {
                  console.log('Error =>', error);
                }, () => {
                  localStorage.setItem('customPassword', this.confirmPasswordField.value);
                  alert('La contraseña se actualizó correctamente');
                  this.formGroup.setValue({ password: '', confirmPassword: '', currentPassword: '' })
                })
            } else {
              alert('La contraseña introducida no es igual a la contraseña actual');
            }
          }
        }
      } else {
        alert('Las contraseñas no son iguales');
        this.confirmChangePass = false;
        AjustesComponent.clic--;
      }
    }

    console.log(JSON.parse(localStorage.getItem('token')).id);

  }

  private startBuildForm(): void {
    this.formGroup = this.formBuilder.group({
      // tslint:disable-next-line: max-line-length
      password: ['', [Validators.required, Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9]).{8,}'), Validators.minLength(8), Validators.maxLength(32)]],
      confirmPassword: ['', [Validators.required]],
      currentPassword: ['']
    });
  }

  // Propiedades get
  get passwordField(): AbstractControl {
    return this.formGroup.get('password');
  }

  get confirmPasswordField(): AbstractControl {
    return this.formGroup.get('confirmPassword');
  }

  get currentPasswordField(): AbstractControl {
    return this.formGroup.get('currentPassword');
  }

}
