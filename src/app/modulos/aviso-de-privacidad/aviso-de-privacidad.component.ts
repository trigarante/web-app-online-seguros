import { Component, OnInit } from '@angular/core';
import {routes} from '../../consts';
import {Router} from '@angular/router';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-aviso-de-privacidad',
  templateUrl: './aviso-de-privacidad.component.html',
  styleUrls: ['./aviso-de-privacidad.component.css']
})
export class AvisoDePrivacidadComponent implements OnInit {
  pdfAviso =  `${environment.apiNode}poliza/aviso-privacidad`;
  private routers: typeof routes = routes;
  mobile = false;
  checked = false;
  constructor(private router: Router) { }

  ngOnInit(): void {
    if (window.screen.width < 450) { // 768px portrait
      this.mobile = true;
    }
  }
  aceptarAviso() {
    localStorage.setItem('aviso', 'true');
    this.router.navigate([this.routers.DASHBOARD]).then();
  }

}
