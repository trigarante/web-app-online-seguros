import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InspeccionVehicularRoutingModule } from './inspeccion-vehicular-routing.module';
import { InspeccionVehicularComponent } from './components/inspeccion-vehicular/inspeccion-vehicular.component';
import { InspeccionVehicularPageComponent } from './containers/inspeccion-vehicular-page/inspeccion-vehicular-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';


@NgModule({
  declarations: [InspeccionVehicularComponent, InspeccionVehicularPageComponent],
  imports: [
    CommonModule,
    InspeccionVehicularRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule
  ]
})
export class InspeccionVehicularModule { }
