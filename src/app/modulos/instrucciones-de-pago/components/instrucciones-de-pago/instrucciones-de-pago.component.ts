import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-instrucciones-de-pago',
  templateUrl: './instrucciones-de-pago.component.html',
  styleUrls: ['./instrucciones-de-pago.component.scss']
})
export class InstruccionesDePagoComponent implements OnInit {
  insurersGroupCollection: Array<{ id: string, nota1: string, nota2: string, urlIMG: string, documento: string, active: boolean }>;
  iter = false;

  constructor() {
    this.insurersGroupCollection = [
      {
        id: 'ABA',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/aba.svg',
        documento: 'assets/docs/aseguradoras/abaSeguros.pdf',
        active: false
      },
      {
        id: 'SEGUROS AFIRME',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/afirme.svg',
        documento: '',
        active: false
      },
      {
        id: 'ANA SEGUROS',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/ana.svg',
        documento: '',
        active: false
      },
      {
        id: 'AXA',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/axa.svg',
        documento: '',
        active: false
      },
      {
        id: 'BANORTE',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/banorte.svg',
        documento: 'assets/docs/aseguradoras/banorte.pdf',
        active: false
      },
      {
        id: 'GENERAL DE SEGUROS',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/general.svg',
        documento: '',
        active: false
      },
      {
        id: 'GNP',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/gnp.svg',
        documento: '',
        active: false
      },
      {
        id: 'HDI',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/hdi.svg',
        documento: '',
        active: false
      },
      {
        id: 'INBURSA',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/inbursa.svg',
        documento: '',
        active: false
      },
      {
        id: 'MAPFRE',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/mapfre.svg',
        documento: '',
        active: false
      },
      {
        id: 'MIGO',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/migo.svg',
        documento: '',
        active: false
      },
      {
        id: 'EL POTOSI',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/elpotosi.svg',
        documento: '',
        active: false
      },
      {
        id: 'QUALITAS',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/qualitas.svg',
        documento: 'assets/docs/aseguradoras/qualitas.pdf',
        active: false
      },
      {
        id: 'ZURA',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/sura.svg',
        documento: '',
        active: false
      },
      {
        id: 'ZURICH',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/zurich.svg',
        documento: '',
        active: false
      },
      {
        id: 'EL AGUILA',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/elaguila.svg',
        documento: 'assets/docs/aseguradoras/aguilaSeguros.pdf',
        active: false
      },
      {
        id: 'AIG',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/aig.svg',
        documento: '',
        active: false
      },
      {
        id: 'LA LATINO',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros al: 5536024275',
        urlIMG: 'assets/img/icons/lalatino.svg',
        documento: '',
        active: false
      },
      {
        id: 'ATLAS SEGUROS',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros',
        urlIMG: 'assets/img/icons/atlas.svg',
        documento: 'assets/docs/aseguradoras/atlasSeguros.pdf',
        active: false
      },
      {
        id: 'Bxmas',
        nota1: 'Recuerda compartirnos tu comprobante de pago',
        nota2: 'Lo sentimos, la aseguradora está en proceso de renovar las cuentas por favor comunicate con nosotros',
        urlIMG: 'assets/img/icons/bxmas.svg',
        documento: 'assets/docs/aseguradoras/Bx+.pdf',
        active: false
      }
    ];
  }

  showContent(i) {
    this.insurersGroupCollection[i].active = !this.insurersGroupCollection[i].active;
  }

  ngOnInit(): void {
  }
}
