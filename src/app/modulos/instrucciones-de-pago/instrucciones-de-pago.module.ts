import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { InstruccionesDePagoRoutingModule } from './instrucciones-de-pago-routing.module';
import { InstruccionesDePagoComponent } from './components/instrucciones-de-pago/instrucciones-de-pago.component';
import { InstruccionesDePagoPageComponent } from './containers/instrucciones-de-pago-page/instrucciones-de-pago-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from "@angular/material/tooltip";
import {PdfViewerModule} from "ng2-pdf-viewer";


@NgModule({
  declarations: [InstruccionesDePagoComponent, InstruccionesDePagoPageComponent],
    imports: [
        CommonModule,
        InstruccionesDePagoRoutingModule,
        SharedModule,
        MatToolbarModule,
        MatCardModule,
        MatIconModule,
        MatTooltipModule,
        PdfViewerModule
    ]
})
export class InstruccionesDePagoModule { }
