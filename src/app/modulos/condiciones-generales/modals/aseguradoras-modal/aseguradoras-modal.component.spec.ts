import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AseguradorasModalComponent } from './aseguradoras-modal.component';

describe('AseguradorasModalComponent', () => {
  let component: AseguradorasModalComponent;
  let fixture: ComponentFixture<AseguradorasModalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AseguradorasModalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AseguradorasModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
