import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CondicionesGeneralesRoutingModule } from './condiciones-generales-routing.module';
import {CondicionesGeneralesComponent} from './components/condiciones-generales/condiciones-generales.component';
import { CondicionesGeneralesPageComponent } from './containers/condiciones-generales-page/condiciones-generales-page.component';
import {SharedModule} from '../../shared/shared.module';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatCardModule} from '@angular/material/card';
import {MatIconModule} from '@angular/material/icon';
import {MatTooltipModule} from '@angular/material/tooltip';
import {PdfViewerModule} from 'ng2-pdf-viewer';
import {MatDialogModule} from '@angular/material/dialog';
import {MatButtonModule} from '@angular/material/button';
import {AseguradorasModalComponent} from './modals/aseguradoras-modal/aseguradoras-modal.component';


@NgModule({
  declarations: [
    CondicionesGeneralesComponent,
    CondicionesGeneralesPageComponent,
    AseguradorasModalComponent,
  ],
  imports: [
    CommonModule,
    CondicionesGeneralesRoutingModule,
    SharedModule,
    MatToolbarModule,
    MatCardModule,
    MatIconModule,
    MatTooltipModule,
    PdfViewerModule,
    MatDialogModule,
    MatButtonModule
  ]
})
export class CondicionesGeneralesModule { }
