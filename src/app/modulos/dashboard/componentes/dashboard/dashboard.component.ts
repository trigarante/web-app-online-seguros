import {Component, Input, OnInit} from '@angular/core';
import {ClienteClases} from '../../../../@core/clases/clienteClases';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit{
  saludo = '';
  user: string;
  clienteClase = new ClienteClases();
  cartaDeBienvenida = 'assets/docs/dashboard/FolletoExplicativo.pdf';
  constructor() {
    this.clienteClase.validarExisteCliente().then(() => {
      this.user = JSON.parse(localStorage.getItem('cliente')).nombre;
    });
  }
  async ngOnInit() {
    this.mostrarSaludo();
  }

  mostrarSaludo(): void{
    const fecha = new Date();
    const hora = fecha.getHours();

    if (hora >= 0 && hora < 12){
      this.saludo = 'Buenos Días.';
    }

    if (hora >= 12 && hora < 18){
      this.saludo = 'Buenas Tardes.';
    }

    if (hora >= 18 && hora < 24){
      this.saludo = 'Buenas Noches.';
    }
  }
}
