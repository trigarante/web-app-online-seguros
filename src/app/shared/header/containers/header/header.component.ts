import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

import { Email, User } from '../../../../modulos/auth/models';
import { AuthService, EmailService } from '../../../../modulos/auth/services';
import { routes } from '../../../../consts';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  @Input() isMenuOpened: boolean;
  @Output() isShowSidebar = new EventEmitter<boolean>();
  public routers: typeof routes = routes;
  public menuOpen = false;
  public routes: typeof routes = routes;
  public isOpenUiElements = false;
  url;
  color: string;
  transform: string;

  constructor(private userService: AuthService, private router: Router) {}

  public signOut(): void {
    this.userService.signOut();

    this.router.navigate([this.routers.LOGIN]);
  }
  toggleMenu() {
    this.menuOpen = !this.menuOpen;
    const sections = document.querySelectorAll('.subMenu-activo');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.remove('subMenu-activo');
    }
  }

  openHijo(event) {
    if (event.target.classList.value === 'elementos-del-menu-1') {
      const sectionsfather = document.querySelectorAll('.menuPrincipal .elementos-del-menu-1');
      for (let i = 0; i < sectionsfather.length; i++) {
        sectionsfather[i].classList.remove('subMenu-activo');
      }
    }
    const sections = document.querySelectorAll('.menu-hijo .subMenu-activo');
    for (let i = 0; i < sections.length; i++) {
      sections[i].classList.remove('subMenu-activo');
    }
    event.target.classList.toggle('subMenu-activo');
  }
  transformStyles(){
    if(this.isOpenUiElements === true){
      this.transform = 'rotate(-0.5turn)';
    }
    else{
      this.transform = 'rotate(0turn)';
    }
    return this.transform;
  }
  changesStyles(){
    if(this.isOpenUiElements === true){
      this.color = '#00000021';
    }
    else{
      this.color = 'transparent';
    }
    return this.color;
  }
  public openUiElements() {
    this.isOpenUiElements = !this.isOpenUiElements;
  }
}
