import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';

import { routes } from '../../../../consts';
import {AuthService} from '../../../../modulos/auth/services';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit{
  @Output() signOut: EventEmitter<void> = new EventEmitter<void>();
  public routes: typeof routes = routes;
  nombreCliente: string;
  public signOutEmit(): void {
    this.signOut.emit();
  }

  constructor(private userService: AuthService) {
    this.nombreCliente = localStorage.getItem('cliente') !== null ?
      JSON.parse(localStorage.getItem('cliente')).nombre : 'undefined';
  }
  ngOnInit() {
    this.getClienteById();
  }

  getClienteById() {
    const cliente = localStorage.getItem('cliente');
    if (cliente === null) {
      this.userService.getUser().catch(() => {}).then(() => {
        this.nombreCliente = JSON.parse(localStorage.getItem('cliente')).nombre;
      });
    }
  }
}
